//
// Created by leonardo on 06/08/2019.
//

#ifndef PIX_IMAGE_RGBPIXEL_H
#define PIX_IMAGE_RGBPIXEL_H
struct RGBPixel{
    explicit RGBPixel(int r=0, int g=0, int b=0) : R(r), G(g), B(b) {}
    RGBPixel (const RGBPixel& pix) : R(pix.R), G(pix.G), B(pix.B){}

    RGBPixel operator = (const RGBPixel &rh){
        if (this != &rh) {
            R = rh.R;
            G = rh.G;
            B = rh.B;
        }
        return (*this);
    }
    int R;
    int G;
    int B;
};
#endif //PIX_IMAGE_RGBPIXEL_H
