//
// Created by leonardo on 06/08/2019.
//

#ifndef PIX_IMAGE_BITMAPIMAGE_H
#define PIX_IMAGE_BITMAPIMAGE_H
#include <iostream>
#include "RGBPixel.h"
class BitmapImage{
public:
    BitmapImage(int x, int y) : width(x), heigth(y){
        if (x>0 && y>0)
            buffer = new RGBPixel[x * y];
        else
            std::cout <<"Error"<<std::endl;
    }

    virtual ~BitmapImage() {delete[] buffer;}
    void setPixel(int x, int y, RGBPixel& pix) {

    };

private:
    int width;
    int heigth;
    RGBPixel* buffer;
};
#endif //PIX_IMAGE_BITMAPIMAGE_H
