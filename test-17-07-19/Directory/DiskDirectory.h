//
// Created by leonardo on 04/08/2019.
//

#ifndef DIRECTORY_DISKDIRECTORY_H
#define DIRECTORY_DISKDIRECTORY_H

#include <list>
#include <string>
#include <iostream>
#include <algorithm>
#include "DiskFile.h"

using namespace std;

class DiskDirectory {
public:

    DiskDirectory(const string &name, const std::list<DiskFile> &fl, const std::list<DiskDirectory> &dl) : name(name), fileList(fl), dirList(dl) {}
    virtual ~DiskDirectory() {}

    bool operator==(const DiskDirectory &rhs) const {
        return name == rhs.name &&
               fileList == rhs.fileList &&
               dirList == rhs.dirList;
    }

    bool operator!=(const DiskDirectory &rhs) const {
        return !(rhs == *this);
    }

    const string &getName() const {
        return name;
    }
    void rename(const string &n) {
        name = n;
    }
    void add(DiskFile& file){
        auto findIter = std::find(fileList.begin(), fileList.end(), file);
        if (findIter == fileList.end())
            fileList.push_back(file);
        else
            std::cout<<"File already existent"<<std::endl<<std::endl;
    }
    void add(DiskDirectory& dir){
        auto findIter = std::find(dirList.begin(), dirList.end(), dir);
        if (findIter == dirList.end())
            dirList.push_back(dir);
        else
            std::cout << "Directory already existent" <<std::endl<<std::endl;
    }
    void remove (string& del) {
        for (auto iter : fileList){
            if (iter.getName() == del){
                std::cout << iter.getName() << "  Eliminated file" << std::endl<<std::endl;
                fileList.remove(iter);
                nFiles--;
            }
        }
        for (auto iter : dirList){
            if (iter.getName() == del){
                std::cout << iter.getName() << "  Eliminated directory" << std::endl<<std::endl;
                dirList.remove(iter);
                nDir--;
            }
        }
    }


    void list() {
        cout << "File list:"<< endl;
        for (auto itr : fileList)
            cout << "      " << itr.getName() << endl;
        cout << "Directory list:"<< endl;
        for (auto itr: dirList)
            cout << "      " << itr.getName() << endl;
        std::cout<<std::endl;
    }
    void print() {
        cout << "Directory:" << name << endl;


        for (auto itr : fileList){
            nFiles++;
            totDim += itr.getBytes();
        }
        cout << "  Number of Files :" << nFiles << endl;
        cout << "  Total Dimension :" << totDim << endl;
        cout<<endl;

        for (auto itr : dirList)
            nDir++;
        cout << "  Number of Directories :" << nDir << endl;
    }

private:
    string name;
    std::list<DiskFile> fileList;
    std::list<DiskDirectory> dirList;
    int totDim = 0;
    unsigned int nFiles = 0;
    unsigned int nDir = 0;
};



#endif //DIRECTORY_DISKDIRECTORY_H
