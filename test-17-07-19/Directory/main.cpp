#include <iostream>
#include <map>
#include "DiskFile.h"
#include "DiskDirectory.h"

int main() {
    DiskFile file1 ("file1", 512, true);
    DiskFile file2 ("file2", 256);
    std::list<DiskFile> fl;
    std::list<DiskFile> fl2;
    std::list<DiskDirectory> dl;
    std::list<DiskDirectory> dl2;
    DiskDirectory dir1 ("dir1", fl, dl);
    DiskDirectory dir2 ("dir2", fl2, dl2);
    dir1.add(file1);
    dir1.add(file2);
    dir1.add(dir2);
    dir1.remove((string &) "file1");
    dir1.remove(((string&) "dir2"));
    dir1.print();


}