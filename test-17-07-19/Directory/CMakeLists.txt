cmake_minimum_required(VERSION 3.14)
project(Directory)

set(CMAKE_CXX_STANDARD 14)

add_executable(Directory main.cpp DiskFile.h DiskDirectory.h)