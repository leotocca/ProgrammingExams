//
// Created by leonardo on 04/08/2019.
//

#ifndef DIRECTORY_DISKFILE_H
#define DIRECTORY_DISKFILE_H

#include <iostream>
#include <string>

class DiskFile {
public:
    DiskFile() : name(""), bytes(0), writable(false){}
    DiskFile(std::string n, int b) : name(n), bytes(b), writable(false){}
    DiskFile(std::string n, int b, bool w) : name(n), bytes(b), writable(w){}
    virtual ~DiskFile() {}

    const std::string &getName() const {
        return name;
    }

    void rename (const std::string &name) {
        DiskFile::name = name;
    }

    bool operator==(const DiskFile &rhs) const {
        return name == rhs.name &&
               bytes == rhs.bytes &&
               writable == rhs.writable;
    }

    bool operator!=(const DiskFile &rhs) const {
        return !(rhs == *this);
    }

    int getBytes() const {
        return bytes;
    }

    void print(){
        std::cout << "Name: " << name << "; Dimension: " << bytes << "; Writable:" << writable << std::endl;
    }
private:
    std::string name;

    int bytes;
    bool writable;

};
#endif //DIRECTORY_DISKFILE_H
