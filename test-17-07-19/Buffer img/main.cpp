#include <iostream>
#include "RGBPixel.h"
#include "ImagePixel.h"

int main() {
    RGBPixel pixel(1,2,3);
    //pixel.setR(1);
    //pixel.setB(9);

    ImagePixel image(30,25);
    image.setPixel(4, 5, pixel);
        //image.getPixel(56, 87);

    std::cout<<image.getPixel(4,5).getR();
}