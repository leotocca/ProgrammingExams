//
// Created by leonardo on 05/08/2019.
//

#ifndef BUFFER_IMG_RGBPIXEL_H
#define BUFFER_IMG_RGBPIXEL_H

#include <iostream>
#include <exception>

struct RGBPixel{
    RGBPixel(): R(0), G(0), B(0) {}
    RGBPixel(int r, int g, int b) : R(r), G(g), B(b) {}
    RGBPixel (const RGBPixel& pix) : R(pix.R), G(pix.G), B(pix.B){}

    RGBPixel operator = (const RGBPixel &rh){
        if (this != &rh) {
            R = rh.R;
            G = rh.G;
            B = rh.B;
        }
        return (*this);
    }

     int getR() const {
        return R;
    }

    void setR(int r) {
        R = r;
    }

    int getG() const {
        return G;
    }

    void setG(int g) {
        G = g;
    }

    int getB() const {
        return B;
    }
    void setB(int b) {
        B = b;
    }

    int R;
    int G;
    int B;
};
#endif //BUFFER_IMG_RGBPIXEL_H
