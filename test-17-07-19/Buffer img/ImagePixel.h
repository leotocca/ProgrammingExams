//
// Created by leonardo on 05/08/2019.
//

#ifndef BUFFER_IMG_IMAGEPIXEL_H
#define BUFFER_IMG_IMAGEPIXEL_H

#include <exception>
#include <stdexcept>
#include <iostream>
#include <vector>
#include <typeinfo>

#include "RGBPixel.h"
using namespace std;

class ImagePixel{
public:
    ImagePixel(int x, int y) : width(x), height(y) {
        bitmap = new RGBPixel[x * y];
        for(int i=0;i<width;i++) {
            for (int j = 0; j < height; j++) {
                bitmap[i + j * width].setR(0);
                bitmap[i + j * width].setG(0);
                bitmap[i + j * width].setB(0);
            }
        }
    }

    virtual ~ImagePixel() {
        delete[] bitmap;
    }

    ImagePixel(const ImagePixel& img){ //copy constructor
        width = img.width;
        height = img.height;
        if (img.bitmap != nullptr)
            bitmap = new RGBPixel(*img.bitmap);
    }

    ImagePixel& operator = (const ImagePixel & rh) { //assign operator
        if (this != &rh) {
            width = rh.width;
            height = rh.height;
            *bitmap = *rh.bitmap;
        }
        return *this;
    }

    void setPixel(int x, int y, RGBPixel pixel) {
        if(x<0 || x>=width || y<0 || y>=height)
            throw out_of_range("Wrong pixel coordinates");
        if(pixel.getR()<0 || pixel.getG()<0 || pixel.getB()<0)
            throw invalid_argument("No pixel there");
        bitmap[x + y*width].setR(pixel.getR());
        bitmap[x + y*width].setG(pixel.getG());
        bitmap[x + y*width].setB(pixel.getB());
        std::cout<<"Pixel impostato alle coordinate:("<<x<<", "<<y<<")"<<std::endl;
    }
    RGBPixel getPixel (int x, int y){
        if(x<0 || x>=width || y<0 || y>=height)
            throw out_of_range("Wrong pixel coordinates");
        return bitmap[x + y*width];
    }
    int getR(int x, int y) const{
        if (x<=width && y<=height){
            return bitmap->getR();
        }
        else
            throw invalid_argument("No pixel there");
    }
    int& getG(int x, int y) const{
        if (x<=width && y<=height){
            return bitmap->G;
        }
        else
            throw invalid_argument("No pixel there");
    }
    int &getB (int x, int y) const{
        if (x<=width && y<=height){
            return bitmap->B;
        }
        else
            throw invalid_argument("No pixel there");
    }


private:
    int width,height;
    RGBPixel* bitmap;
};

#endif //BUFFER_IMG_IMAGEPIXEL_H
