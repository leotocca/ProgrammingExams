//
// Created by leonardo on 17/07/19.
//

#ifndef OBSERVER_MULTIMONITOR_H
#define OBSERVER_MULTIMONITOR_H

#include <iostream>
#include <typeinfo>
#include <string>
#include <memory>
#include "Observer.h"
#include "PacketMon.h"
#include "CPUUSage.h"


class MultiMonitor : public Observer{
public:
    MultiMonitor (PacketMon* s1,CPUUsage* s2, std::string monitorName): subj1(s1), subj2(s2), name(monitorName){
        attach();

    };
    ~MultiMonitor() override {
        detach();

    }
    void update () override {
        std::cout<< name << ":: Packages"<< ": " << subj1->getPackets()<< "; Usage"<< ": " <<subj2->getUsage()<<std::endl;

    }
    void attach() override {
        subj1->subscribe(this);
        subj2->subscribe(this);
    }
    void detach() override{
        subj1->unsubscribe(this);
        subj2->unsubscribe(this);
    }

private:
    std::string name;
    PacketMon* subj1;
    CPUUsage* subj2;

};
#endif //OBSERVER_MULTIMONITOR_H
