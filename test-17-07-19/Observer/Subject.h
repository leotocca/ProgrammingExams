//
// Created by leonardo on 17/07/19.
//

#ifndef OBSERVER_SUBJECT_H
#define OBSERVER_SUBJECT_H

#include "Observer.h"

class Subject{
public:
    virtual ~Subject() {}
    virtual void subscribe (Observer* o) = 0;
    virtual void unsubscribe (Observer* o) = 0;
    virtual void notify() = 0;
};


#endif //OBSERVER_SUBJECT_H
