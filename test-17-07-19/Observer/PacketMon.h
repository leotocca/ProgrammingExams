//
// Created by leonardo on 17/07/19.
//

#ifndef OBSERVER_PACKETMON_H
#define OBSERVER_PACKETMON_H

#include "Subject.h"
#include <list>
class PacketMon : public Subject {
public:
    explicit PacketMon () {
        packets = 0;
    }
    ~PacketMon() override {};

    void addPacket(int p){
        this->packets += p;
        notify();
    }
    int getPackets(){
        return packets;
    }

    void subscribe (Observer* o) override {
        observers.push_back(o);
    }
    void unsubscribe (Observer* o) override {
        observers.remove(o);
    }
    void notify() override {
        for (auto intr : observers){
            if (packets%100 == 0)
                (*intr).update();
        }
    }

private:
    int packets;
    std::list<Observer*>observers;
};
#endif //OBSERVER_PACKETMON_H
