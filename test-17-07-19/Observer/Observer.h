//
// Created by leonardo on 17/07/19.
//

#ifndef OBSERVER_OBSERVER_H
#define OBSERVER_OBSERVER_H

#include <list>
#include "Subject.h"

class Observer {
public:
    virtual ~Observer() {}
    virtual void attach () = 0;
    virtual void detach () = 0;
    virtual void update () = 0;

};

#endif //OBSERVER_OBSERVER_H
