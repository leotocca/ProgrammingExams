//
// Created by leonardo on 17/07/19.
//

#ifndef OBSERVER_CPUUSAGE_H
#define OBSERVER_CPUUSAGE_H

#include "Subject.h"
#include <list>
class CPUUsage : public Subject {
public:
    explicit CPUUsage () : use (0) {}
    virtual ~CPUUsage() {};

    void update(int howMuch = 1){
        use +=howMuch;
        notify();
    }
    int getUsage(){
        return use;
    }

    virtual void subscribe (Observer* o) override {
        observers.push_back(o);
    }
    virtual void unsubscribe (Observer* o) override {
        observers.remove(o);
    }
    virtual void notify() override
    {
        for (auto intr : observers){
            if (use%10 == 0)
                (*intr).update();
        }
    }
private:
    int use;
    std::list<Observer*>observers;
};
#endif //OBSERVER_CPUUSAGE_H
