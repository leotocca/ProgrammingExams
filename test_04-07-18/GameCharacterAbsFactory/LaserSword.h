//
// Created by leonardo on 10/09/2019.
//

#ifndef GAMECHARACTERABSFACTORY_LASERSWORD_H
#define GAMECHARACTERABSFACTORY_LASERSWORD_H

#include <iostream>
#include "Weapon.h"

class LaserSword : public  Weapon{
public:
    explicit LaserSword(int strength=6, bool shield=false) {
        this->strength = strength;
        this->shield = shield;
    }

    void setStrength (int s){
        this->strength = s;
    }

     int use() override {
        std::cout << "Using laser sword." << std::endl;
    }
};


#endif //GAMECHARACTERABSFACTORY_LASERSWORD_H
