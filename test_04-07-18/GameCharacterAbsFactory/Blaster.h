//
// Created by leonardo on 10/09/2019.
//

#ifndef GAMECHARACTERABSFACTORY_BLASTER_H
#define GAMECHARACTERABSFACTORY_BLASTER_H

#include <iostream>
#include "Weapon.h"

class Blaster : public Weapon {
public:
    explicit Blaster(int s, bool sh=false) {
        this->strength = s;
        this->shield = sh;
    }

    void setStrength (int s){
        this->strength = s;
    }
    int use() override{
        std::cout << "Using blaster." << std::endl;
    }
};


#endif //GAMECHARACTERABSFACTORY_BLASTER_H
