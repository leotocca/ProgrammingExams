//
// Created by leonardo on 10/09/2019.
//

#ifndef GAMECHARACTERABSFACTORY_JEDIKNIGHT_H
#define GAMECHARACTERABSFACTORY_JEDIKNIGHT_H


#include "GameCharacter.h"

class JediKnight : public GameCharacter{
public:
    explicit JediKnight (Weapon* weap = nullptr) {
        this->posX = 0;
        this->posY = 0;
        this->w = weap;
    }

    int fight (GameCharacter & enemy)  override {}
    void move (int x, int y ) override {}
};


#endif //GAMECHARACTERABSFACTORY_JEDIKNIGHT_H
