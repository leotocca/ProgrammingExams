//
// Created by leonardo on 10/09/2019.
//

#ifndef GAMECHARACTERABSFACTORY_STORMTROOPER_H
#define GAMECHARACTERABSFACTORY_STORMTROOPER_H


#include "GameCharacter.h"

class StormTrooper : public GameCharacter{
public:
    explicit StormTrooper (Weapon* weap = nullptr) {
        this->posX = 0;
        this->posY = 0;
        this->w = weap;
    }

    int fight (GameCharacter & enemy)  override {}
    void move (int x, int y ) override {}
};


#endif //GAMECHARACTERABSFACTORY_STORMTROOPER_H
