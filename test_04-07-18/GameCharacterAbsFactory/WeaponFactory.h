//
// Created by leonardo on 10/09/2019.
//

#ifndef GAMECHARACTERABSFACTORY_WEAPONFACTORY_H
#define GAMECHARACTERABSFACTORY_WEAPONFACTORY_H

#include "AbstractFactory.h"
#include "LaserSword.h"
#include "Blaster.h"

class WeaponFactory : public AbstractFactory{
public:
    virtual GameCharacter* createCharacter(std::string charType) override{}
    virtual Weapon* createWeapon(std::string weaponType) override{
        Weapon* weapon;
        if(weaponType == "l"){
            weapon = new LaserSword();
            std::cout << "Laser sword created." << std::endl;
        }else if(weaponType == "b"){
            weapon = new Blaster(0);
            std::cout << "Blaster created." << std::endl;
        }else{
            weapon = nullptr;
            std::cout << "No weapon is created." << std::endl;
        }
        return weapon;
    }
};


#endif //GAMECHARACTERABSFACTORY_WEAPONFACTORY_H
