#include <iostream>
#include <string>
#include "ProduceFactory.h"


int main() {

    //Getting weapon factory through produce factory
    ProduceFactory* produceFactory = new ProduceFactory;

    AbstractFactory* weaponFactory;
    weaponFactory = produceFactory->getFactory("w");

    //Getting weapons
    Weapon* blaster;
    blaster = weaponFactory->createWeapon("b");
    blaster->use();

    Weapon* laserSword;
    laserSword = weaponFactory->createWeapon("l");
    laserSword->use();
    std::cout << std::endl;
    //Getting game character factory through produce Factory
    AbstractFactory* gameCharacterFactory;
    gameCharacterFactory = produceFactory->getFactory("c");

    //Getting characters
    GameCharacter* jediKnight;
    jediKnight = gameCharacterFactory->createCharacter("j");
    jediKnight->move(5,5);

    GameCharacter* rebelGuard;
    rebelGuard = gameCharacterFactory->createCharacter("r");
    rebelGuard->move(4,5);

    GameCharacter* stormTrooper;
    stormTrooper = gameCharacterFactory->createCharacter("s");
    stormTrooper->move(2,3);
    std::cout << std::endl;

    jediKnight->fight(*rebelGuard);
    rebelGuard->fight(*jediKnight);
}