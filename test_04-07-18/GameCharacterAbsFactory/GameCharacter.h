//
// Created by leonardo on 10/09/2019.
//

#ifndef GAMECHARACTERABSFACTORY_GAMECHARACTER_H
#define GAMECHARACTERABSFACTORY_GAMECHARACTER_H

#include "Weapon.h"

class GameCharacter {
public:
    virtual ~GameCharacter(){}

    virtual int fight (GameCharacter & enemy) = 0;
    virtual void move (int x, int y) = 0;

    Weapon* w;
    int posX, posY;
};


#endif //GAMECHARACTERABSFACTORY_GAMECHARACTER_H
