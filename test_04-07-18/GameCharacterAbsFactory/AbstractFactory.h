//
// Created by leonardo on 10/09/2019.
//

#ifndef GAMECHARACTERABSFACTORY_ABSTRACTFACTORY_H
#define GAMECHARACTERABSFACTORY_ABSTRACTFACTORY_H

#include <string>
#include "GameCharacter.h"
#include "Weapon.h"
#include "ProduceFactory.h"

class AbstractFactory{
public:
    virtual GameCharacter* createCharacter(std::string charType) = 0;
    virtual Weapon* createWeapon(std::string weaponType) = 0;
};


#endif //GAMECHARACTERABSFACTORY_ABSTRACTFACTORY_H
