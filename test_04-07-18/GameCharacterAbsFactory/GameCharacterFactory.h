//
// Created by leonardo on 10/09/2019.
//

#ifndef GAMECHARACTERABSFACTORY_GAMECHARACTERFACTORY_H
#define GAMECHARACTERABSFACTORY_GAMECHARACTERFACTORY_H


#include "JediKnight.h"
#include "RebelGuard.h"
#include "StormTrooper.h"
#include "AbstractFactory.h"

class GameCharacterFactory :public AbstractFactory {
public:
    virtual GameCharacter* createCharacter(std::string charType) override {
        GameCharacter* gameChar;
        if(charType == "j"){
            gameChar = new JediKnight;
            std::cout << "Jedi knight created" << std::endl;
        }
        else if(charType == "r" ){
            gameChar = new RebelGuard;
            std::cout << "Rebel guard created." << std::endl;
        }
        else if(charType == "s"){
            gameChar = new StormTrooper;
            std::cout << "Storm trooper created." << std::endl;
        }
        else {
            gameChar = nullptr;
            std::cout << "No character is created." << std::endl;
        }
        return gameChar;
    }
    virtual Weapon* createWeapon(std::string weaponType) override{
    }
};


#endif //GAMECHARACTERABSFACTORY_GAMECHARACTERFACTORY_H
