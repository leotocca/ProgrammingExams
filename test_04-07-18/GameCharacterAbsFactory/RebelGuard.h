//
// Created by leonardo on 10/09/2019.
//

#ifndef GAMECHARACTERABSFACTORY_REBELGUARD_H
#define GAMECHARACTERABSFACTORY_REBELGUARD_H


#include "GameCharacter.h"

class RebelGuard : public GameCharacter{
public:
    explicit RebelGuard (Weapon* weap = nullptr) {
        this->posX = 0;
        this->posY = 0;
        this->w = weap;
    }

    int fight (GameCharacter & enemy)  override {}
    void move (int x, int y ) override {}
};


#endif //GAMECHARACTERABSFACTORY_REBELGUARD_H
