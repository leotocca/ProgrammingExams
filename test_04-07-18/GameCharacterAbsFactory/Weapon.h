//
// Created by leonardo on 10/09/2019.
//

#ifndef GAMECHARACTERABSFACTORY_WEAPON_H
#define GAMECHARACTERABSFACTORY_WEAPON_H



class Weapon {
public:
    virtual ~Weapon(){}

    virtual int use() {}

    int strength;
    bool shield;
};


#endif //GAMECHARACTERABSFACTORY_WEAPON_H
