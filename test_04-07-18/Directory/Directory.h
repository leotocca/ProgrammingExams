//
// Created by leonardo on 10/09/2019.
//

#ifndef DIRECTORY_DIRECTORY_H
#define DIRECTORY_DIRECTORY_H

#include <list>
#include "File.h"

class Directory {
public:
    Directory(std::string n, int day, int month, int year, int ora, int min, int sec) {
        this->name = n;
        Date data;
        data.day = day;
        data.month = month;
        data.year = year;
        data.hour = ora;
        data.min = min;
        data.sec = sec;
        this->creation = data;
        this->edit = data;
        this->access = data;
    }
    Directory(std::string n, Date& data){
        this->name = n;
        this->creation = data;
        this->edit = data;
        this->access = data;
    }

    void add (File & f){
        files.push_back(f);
    }

    void remove (File& f){
        for (auto file: files){
            if (file == f)
                files.remove(file);
        }
    }

    void print() const{
        for (auto file : files)
            file.print();
    }

private:
    std::string name;
    Date creation, edit, access;
    std::list<File> files;
};

#endif DIRECTORY_DIRECTORY_H
