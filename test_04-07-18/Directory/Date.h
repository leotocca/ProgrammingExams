//
// Created by leonardo on 10/09/2019.
//

#ifndef DIRECTORY_DATE_H
#define DIRECTORY_DATE_H

#include <iostream>

class Date{
public:
    Date() : day(0), month(0), year(0), hour(0), min(0), sec(0){}
    Date(int d, int mo, int y, int h, int mi, int s) : day(d), month(mo), year(y), hour(h), min(mi), sec(s){}

    void printDate() const{
        std::cout << day << "/"<< month << "/"<< year << " : " << hour << ":" << min << ":" << sec;
    }

    bool operator==(const Date &rhs) const {
        return day == rhs.day &&
               month == rhs.month &&
               year == rhs.year &&
               hour == rhs.hour &&
               min == rhs.min &&
               sec == rhs.sec;
    }

    bool operator!=(const Date &rhs) const {
        return !(rhs == *this);
    }

    int day, month, year, hour, min, sec;
};




#endif //DIRECTORY_DATE_H
