//
// Created by leonardo on 10/09/2019.
//

#ifndef DIRECTORY_FILE_H
#define DIRECTORY_FILE_H


#include <string>
#include "Date.h"

class File {
public:
    explicit File(std::string name, std::string est, int day, int month, int year, int ora, int min, int sec) {
        this->name = name;
        this->extension = est;
        Date data;
        data.day = day;
        data.month = month;
        data.year = year;
        data.hour = ora;
        data.min = min;
        data.sec = sec;
        this->creation = data;
        this->lastEdit = data;
        this->lastAcc = data;
    }

    File (std::string name, std::string est, Date& d){
        this->name = name;
        this->extension = est;
        this->creation = d;
        this->lastEdit = d;
        this->lastAcc = d;
    }

    bool operator==(const File &rhs) const {
        return name == rhs.name && extension == rhs.extension && creation == rhs.creation &&
               lastAcc == rhs.lastAcc &&
               lastEdit == rhs.lastEdit;
    }

    bool operator!=(const File &rhs) const {
        return !(rhs == *this);
    }

    void print() const {
        std::cout << name <<"." << extension  <<"; ";
        std::cout << "creation date"  <<"; ";
        creation.printDate();
    }

private:
    std::string name, extension;
    Date creation, lastAcc, lastEdit;

};



#endif //DIRECTORY_FILE_H
