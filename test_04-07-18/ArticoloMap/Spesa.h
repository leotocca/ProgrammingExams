//
// Created by leonardo on 10/09/2019.
//

#ifndef ARTICOLOMAP_SPESA_H
#define ARTICOLOMAP_SPESA_H

#include <map>
#include <string>
#include <iostream>

struct Articolo {
public:
    Articolo (std::string n, int q) : nome(n), qty(q){}

    bool operator<(const Articolo &rhs) const {
        if (nome < rhs.nome)
            return true;
        if (rhs.nome < nome)
            return false;
        return qty < rhs.qty;
    }

    bool operator>(const Articolo &rhs) const {
        return rhs < *this;
    }

    bool operator<=(const Articolo &rhs) const {
        return !(rhs < *this);
    }

    bool operator>=(const Articolo &rhs) const {
        return !(*this < rhs);
    }

    void print() const {
        std::cout << "    Articolo:" << nome << "; quantità: " << qty << std::endl;
    }

    std::string nome;
    int qty;

};

class Spesa {
public:
    void add(int q,Articolo& a){
        auto it = lista.begin();
        it = lista.find(a);
        if (it != lista.end())
            it->second = q;
        else
            lista.insert(std::make_pair(a,q));

    }

    void remove (std::string r) {
        for (auto &itr : lista) {
            if (itr.first.nome == r) {
                lista.erase(itr.first);
            }
        }
    }

    void print () const {
        std::cout << "Articoli nella lista della spesa:" << std::endl;
        for (auto &itr : lista) {
            itr.first.print();
        }
        std::cout << std::endl;
    }

private:
    std::map <Articolo, int> lista;

};


#endif //ARTICOLOMAP_SPESA_H
