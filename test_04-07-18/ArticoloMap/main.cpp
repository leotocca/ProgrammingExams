#include <iostream>
#include "Spesa.h"

int main() {
    Articolo pane ("pane", 2);
    Articolo latte ("latte", 4);
    Spesa sp;
    sp.add(pane.qty, pane);
    sp.add(latte.qty, latte);
    sp.print();
    sp.remove("pane");
    sp.print();
}