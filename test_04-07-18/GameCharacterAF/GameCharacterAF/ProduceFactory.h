//
// Created by leonardo on 10/09/2019.
//

#ifndef GAMECHARACTERABSFACTORY_PRODUCEFACTORY_H
#define GAMECHARACTERABSFACTORY_PRODUCEFACTORY_H

#include "AbstractFactory.h"
#include "GameCharacterFactory.h"
#include "WeaponFactory.h"

class ProduceFactory{
public:
    AbstractFactory* getFactory(std::string factoryType){
        if(factoryType.compare("c")==0) {
            std::cout << "Character factory selected." << std::endl;
            return new GameCharacterFactory;
        } else if(factoryType.compare("w") == 0){
            std::cout << "Weapon factory selected." << std::endl;
            return new WeaponFactory;
        }
        std::cerr << "No factory created!" << std::endl;
        return nullptr;
    }

};

#endif //GAMECHARACTERABSFACTORY_PRODUCEFACTORY_H
