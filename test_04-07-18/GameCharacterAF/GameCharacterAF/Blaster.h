//
// Created by leonardo on 10/09/2019.
//

#ifndef GAMECHARACTERABSFACTORY_BLASTER_H
#define GAMECHARACTERABSFACTORY_BLASTER_H

#include <iostream>
#include "Weapon.h"

class Blaster : public Weapon {
public:
    explicit Blaster(int strength, bool shield=false) {
        this->strength = strength;
        this->shield = shield;
    }
    int use(GameCharacter& enemy) override{
        std::cout << "Using blaster." << std::endl;
    }
};


#endif //GAMECHARACTERABSFACTORY_BLASTER_H
