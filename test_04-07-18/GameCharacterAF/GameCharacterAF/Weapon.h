//
// Created by leonardo on 10/09/2019.
//

#ifndef GAMECHARACTERABSFACTORY_WEAPON_H
#define GAMECHARACTERABSFACTORY_WEAPON_H


#include "GameCharacter.h"

class Weapon {
public:
    virtual ~Weapon(){}

    virtual int use(GameCharacter& enemy) {}

    int strength;
    bool shield;
};


#endif //GAMECHARACTERABSFACTORY_WEAPON_H
