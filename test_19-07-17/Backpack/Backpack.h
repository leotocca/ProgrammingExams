//
// Created by leonardo on 09/09/2019.
//

#ifndef BACKPACK_BACKPACK_H
#define BACKPACK_BACKPACK_H

#include <list>
#include <iterator>
#include <iostream>

template <typename T>
class Backpack {
public:

    explicit Backpack(int d) : tDimension(d), currDim(d), lastDim(0){}

    bool add (T obj,const int d){
        if (d > currDim || d>tDimension) {
            std::cout << "no space in backpack" << std::endl;
            return false;
        }
        objects.push_back(obj);
        lastDim = d;
        currDim -= d;
        //std::cout << dimension <<std::endl;
        std::cout << "added " << obj << ", dimension: " << d << std::endl;
        return true;
    }
    void remove(){
        currDim += lastDim;
        objects.erase(objects.begin());
    }


    //aggiunto

    void print(){
        std::cout << std::endl  << "list of contents: " << std::endl;
        for (auto& ob : objects){
            std::cout << "objects " << ob << std::endl;
        }
        std::cout << "Remaining space: " << currDim << std::endl;
    }
private:
    std::list<T> objects;
    int tDimension, currDim, lastDim;
};


#endif //BACKPACK_BACKPACK_H
