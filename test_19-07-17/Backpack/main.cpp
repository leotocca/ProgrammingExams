#include <string>
#include "Backpack.h"

using namespace std;

int main() {
    Backpack <string> back(100);
    back.add("boh",50);
    back.add("boh^2",90);
    back.print();
    back.remove();
    back.print();
}