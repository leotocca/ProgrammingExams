//
// Created by leonardo on 08/09/2019.
//

#ifndef FILESYSTEM_WINFACTORY_H
#define FILESYSTEM_WINFACTORY_H

#include "WinDirectory.h"
#include "WinFile.h"
#include "BaseFactory.h"

class WinFactory : public BaseFactory {
public:
    virtual ~WinFactory(){}

    File* createFile (string s, int m) override{
        return new WinFile(s,m);
    }
    Directory* createDirectory (string s) override {
        return new WinDirectory(s);
    }
};


#endif //FILESYSTEM_WINFACTORY_H
