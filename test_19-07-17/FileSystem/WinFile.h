//
// Created by leonardo on 08/09/2019.
//

#ifndef FILESYSTEM_WINFILE_H
#define FILESYSTEM_WINFILE_H

#include "File.h"

class WinFile : public File{
public:
    WinFile(std::string n, int d) : hidden(false){
        create(n);
        numBytes = d;
    }

    virtual ~WinFile(){}

    bool isHidden() const override {
        return hidden;
    }

    void setHidden(bool h) {
        hidden = h;
    }

    bool create (std::string aName) override {
        if (aName.size() <= 0) {
            return false;
        }
        name = aName;
        std::cout << "Created a windows file named: " << name << std::endl;
        return true;
    }

    bool move (std::string newName) override  {
        if (newName.size() <= 0)
            return false;
        name = newName;
        std::cout << "Moved a windows file named: " << name << std::endl;
        return true;
    }

private:
    bool hidden;
};


#endif //FILESYSTEM_WINFILE_H
