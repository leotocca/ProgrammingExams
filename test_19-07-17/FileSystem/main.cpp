#include <iostream>
#include "LinuxFactory.h"
#include "WinFactory.h"

int main() {
    BaseFactory* factory;
    bool isWindows = false;
#ifdef _WIN64
    isWindows == true;
#else
    isWindows == false;
#endif
    if (isWindows == true)
        factory = new WinFactory;
    else
        factory = new LinuxFactory;

    File* file = factory->createFile("name",10);
    Directory* directory = factory->createDirectory("directory");
    directory->addFile(file);
    directory->list();
    file->move("name2");
    directory->list();
    directory->removeFile(file);
    delete directory;
    delete file;
}