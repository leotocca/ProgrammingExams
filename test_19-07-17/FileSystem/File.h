//
// Created by leonardo on 08/09/2019.
//

#ifndef FILESYSTEM_FILE_H
#define FILESYSTEM_FILE_H

#include "FileSystemObject.h"

class File : public FileSystemObject {
public:
    virtual ~File(){}
    virtual bool isHidden() const = 0;

    int numBytes;
};


#endif //FILESYSTEM_FILE_H
