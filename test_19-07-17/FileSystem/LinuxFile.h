//
// Created by leonardo on 08/09/2019.
//

#ifndef FILESYSTEM_LINUXFILE_H
#define FILESYSTEM_LINUXFILE_H

#include "File.h"
#include <cmath>

class LinuxFile : public File{
public:
    LinuxFile(std::string n, int d){
        create(n);
        numBytes = d;
    }

    virtual ~LinuxFile(){}

    bool isHidden() const override {
        if (name.at(0) == '_')
            return true;
    }

    bool create (std::string aName) override {
        if (aName.size() <= 0) {
            return false;
        }
        name = aName;
        std::cout << "Created a linux file named: " << name << std::endl;
        return true;
    }

    bool move (std::string newName) override  {
        if (newName.size() <= 0)
            return false;
        name = newName;
        std::cout << "Moved a linux file named: " << name << std::endl;
        return true;
    }
};


#endif //FILESYSTEM_LINUXFILE_H
