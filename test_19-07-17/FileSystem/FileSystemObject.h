//
// Created by leonardo on 08/09/2019.
//

#ifndef FILESYSTEM_FILESYSTEMOBJECT_H
#define FILESYSTEM_FILESYSTEMOBJECT_H

#include <string>
#include <iostream>

class FileSystemObject {
public:
    virtual ~FileSystemObject(){}
    virtual bool create (std::string aName) = 0;
    virtual bool move (std::string newName) = 0;

    std::string name;
};


#endif //FILESYSTEM_FILESYSTEMOBJECT_H
