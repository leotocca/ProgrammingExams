//
// Created by leonardo on 08/09/2019.
//

#ifndef FILESYSTEM_WINDIRECTORY_H
#define FILESYSTEM_WINDIRECTORY_H

#include "Directory.h"

class WinDirectory : public Directory{
public:
    WinDirectory (std::string n) {
        create(n);
    }
    virtual ~WinDirectory(){}

    bool create (std::string aName) override{
        if (aName.size() <= 0) {
            return false;
        }
        name = aName;
        std::cout << "Created a windows directory named: " << name << std::endl;
        return true;
    }

    bool move (std::string newName) override{
        if (newName.size() <= 0)
            return false;
        name = newName;
        std::cout << "Moved a windows directory named: " << name << std::endl;
        return true;
    }
        
    void list() override {
        for (auto& file : files){
            std::cout << file->name << std::endl;
        }
    }
};


#endif //FILESYSTEM_WINDIRECTORY_H
