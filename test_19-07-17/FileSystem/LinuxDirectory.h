//
// Created by leonardo on 08/09/2019.
//

#ifndef FILESYSTEM_LINUXDIRECTORY_H
#define FILESYSTEM_LINUXDIRECTORY_H

#include "Directory.h"

class LinuxDirectory : public Directory{
public:

    LinuxDirectory (std::string n) {
        create(n);
    }
    virtual ~LinuxDirectory(){}

    bool create (std::string aName) override{
        if (aName.size() <= 0) {
            return false;
        }
        name = aName;
        std::cout << "Created a linux directory named: " << name << std::endl;
        return true;
    }

    bool move (std::string newName) override{
        if (newName.size() <= 0)
            return false;
        name = newName;
        std::cout << "Moved a linux directory named: " << name << std::endl;
        return true;
    }

    void list() override {
        files.reverse();
        //for (std::list<File>::const_iterator file = files.end(); file != files.begin(); file-- )
        for (auto& file : files){
            std::cout << file->name << std::endl;
        }
    }
};


#endif //FILESYSTEM_LINUXDIRECTORY_H
