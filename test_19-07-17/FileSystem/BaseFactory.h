//
// Created by leonardo on 08/09/2019.
//

#ifndef FILESYSTEM_BASEFACTORY_H
#define FILESYSTEM_BASEFACTORY_H

#include "File.h"
#include "Directory.h"

using namespace std;

class BaseFactory {
public:
    virtual ~BaseFactory(){}

    virtual File* createFile (string s, int m)= 0;
    virtual Directory* createDirectory (string s) = 0;
};


#endif //FILESYSTEM_BASEFACTORY_H
