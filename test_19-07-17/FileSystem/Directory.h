//
// Created by leonardo on 08/09/2019.
//

#ifndef FILESYSTEM_DIRECTORY_H
#define FILESYSTEM_DIRECTORY_H

#include <list>
#include "File.h"
#include "FileSystemObject.h"

class Directory : public FileSystemObject {
public:
    virtual ~Directory(){}

    void addFile(File* newFile){
        files.push_back(newFile);
    }
    void removeFile (File* oldFile){
        files.remove(oldFile);
    }
    virtual void list() = 0;

protected:
    std::list<File*> files;
};


#endif //FILESYSTEM_DIRECTORY_H
