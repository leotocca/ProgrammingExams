//
// Created by leonardo on 08/09/2019.
//

#ifndef FILESYSTEM_LINUXFACTORY_H
#define FILESYSTEM_LINUXFACTORY_H


#include "BaseFactory.h"
#include "LinuxFile.h"
#include "LinuxDirectory.h"

class LinuxFactory : public BaseFactory{
public:
    virtual ~LinuxFactory(){}

    File* createFile (string s, int m) override{
        return new LinuxFile(s,m);
    }
    Directory* createDirectory(string s) override {
        return new LinuxDirectory(s);
    }
};


#endif //FILESYSTEM_LINUXFACTORY_H
