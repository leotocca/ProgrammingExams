//
// Created by leonardo on 10/09/2019.
//

#ifndef LIBRARY_DVD_H
#define LIBRARY_DVD_H


#include <string>
#include "AbsLib.h"


class DVD : public AbsLib{
public:
    DVD(std::string t, std::string d, int n) : title(t), director(d), duration(n){}
    ~DVD(){}

    void print() override {
        std::cout << "Title: " << title << "; Director:" << director << "; duration in seconds: " << duration
                  << std::endl;
    }
private:
    std::string title, director;
    int duration;
};


#endif //LIBRARY_DVD_H
