//
// Created by leonardo on 10/09/2019.
//

#ifndef LIBRARY_LIBRARY_H
#define LIBRARY_LIBRARY_H

#include <list>
#include <iostream>
#include <string>
#include <unordered_map>
#include "Book.h"
#include "DVD.h"

class Library {
public:
    void add (AbsLib* b){
        list.push_back(b);;
    }

    void remove (AbsLib* a){
        list.remove(a);
    }

    void print () {
        std::cout << "Library;" << std::endl << std::endl;
        for (const auto & itr : list)
            itr->print();
    }

private:
    std::list <AbsLib*> list;
};



#endif //LIBRARY_LIBRARY_H
