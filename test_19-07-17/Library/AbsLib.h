//
// Created by leonardo on 10/09/2019.
//

#ifndef LIBRARY_ABSLIB_H
#define LIBRARY_ABSLIB_H


class AbsLib {
public:
    virtual ~AbsLib(){}
    virtual void print() = 0;
};


#endif //LIBRARY_ABSLIB_H
