//
// Created by leonardo on 10/09/2019.
//

#ifndef LIBRARY_BOOK_H
#define LIBRARY_BOOK_H


#include <string>
#include "AbsLib.h"

class Book : public AbsLib {
public:
    Book(std::string t, std::string a, std::string p, int n) : title(t), author(a), publisher(p), nPages(n){}
    ~Book(){}

    void print() override{
        std::cout << "Title: " << title << "; Author:" << author << "; Publisher:" << publisher << "; number of pages:" << nPages << std::endl;
    }
private:
    std::string title, author, publisher;
    int nPages;
};


#endif //LIBRARY_BOOK_H
