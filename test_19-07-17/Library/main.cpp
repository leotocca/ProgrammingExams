#include <iostream>
#include "Book.h"
#include "DVD.h"
#include "Library.h"

int main() {
    Book firstBook("First book", "Emma Author", "Emma actor", 100);
    DVD firstDvd("First dvd", "Emma dvd", 50);

    Library* library = new Library();

    library->add(&firstBook);
    library->add(&firstDvd);

    library->print();

    library->remove(&firstBook);
    library->print();
}