#include <iostream>
#include <string>
#include "Alarm.h"
#include "AlarmControlCenter.h"
#include "Camera.h"
#include "VolumetricSensor.h"
#include "WindowSensor.h"

int main() {
    Alarm* al1 = new VolumetricSensor ("Front door");
    AlarmControlCenter alc1;
    alc1.addAlarmDevice(al1);
    alc1.anomaly();

    Alarm* al2 = new Camera("Kitchen");
    alc1.addAlarmDevice(al2);
    alc1.anomaly();
}