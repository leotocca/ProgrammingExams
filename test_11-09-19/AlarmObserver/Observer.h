//
// Created by leonardo on 11/09/2019.
//

#ifndef ALARMOBSERVER_OBSERVER_H
#define ALARMOBSERVER_OBSERVER_H

#include <list>
#include "string"
#include "Alarm.h"
#include "Subject.h"

class Subject;

class Observer {
public:
    virtual ~Observer(){}
    virtual void update() = 0;
};



#endif //ALARMOBSERVER_OBSERVER_H
