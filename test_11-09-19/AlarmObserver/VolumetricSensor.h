//
// Created by leonardo on 11/09/2019.
//

#ifndef ALARMOBSERVER_VOLUMETRICSENSOR_H
#define ALARMOBSERVER_VOLUMETRICSENSOR_H

#include <list>
#include <random>
#include "Alarm.h"
#include "Observer.h"

class VolumetricSensor : public Alarm {
public:
    explicit VolumetricSensor (std::string position) : Alarm(position){}
    virtual ~VolumetricSensor(){}

    virtual std::string getAlarmDescription() const override{
        return (position + " volumetric sensor detected a presence of size ");

    }

    int getObjectSize() const {
        std::random_device rd;
        std::mt19937 gen(rd());
        std::uniform_int_distribution<> dis (1,3);
        return dis(gen);
    }

};


#endif //ALARMOBSERVER_VOLUMETRICSENSOR_H
