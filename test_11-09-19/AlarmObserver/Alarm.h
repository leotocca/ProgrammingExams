//
// Created by leonardo on 11/09/2019.
//

#ifndef ALARMOBSERVER_ALARM_H
#define ALARMOBSERVER_ALARM_H


#include <string>
#include <list>
#include "Observer.h"
#include "Subject.h"

class Alarm : public Subject {
public:
    explicit Alarm (std::string p) : position(p) {}
    virtual ~Alarm(){}

    virtual std::string getAlarmDescription() const = 0;
    virtual void activate() final {
        //DONE implement
        for (auto& observer : observers){
            observer->update();
        }

    }
    virtual void subscribe(Observer* o) final {
        observers.push_back(o);
    }
    virtual void unsubscribe(Observer* o) final {
        observers.remove(o);
    }
protected:
    std::string position;
    std::list<Observer*>observers;
};


#endif //ALARMOBSERVER_ALARM_H
