//
// Created by leonardo on 11/09/2019.
//

#ifndef ALARMOBSERVER_CAMERA_H
#define ALARMOBSERVER_CAMERA_H


#include "Alarm.h"
#include "Observer.h"
#include <list>;

class Camera : public Alarm{
public:
    explicit Camera (std::string position) : Alarm(position){    }
    virtual ~Camera(){}

    virtual std::string getAlarmDescription() const override{
        return (position + " camera detected an anomaly");
        getVideoStream();
    }
    std::string getVideoStream() const {
        return  ("Lets pretend that this is a video stream from " + position + " camera");
    }


};


#endif //ALARMOBSERVER_CAMERA_H
