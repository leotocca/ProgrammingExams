//
// Created by leonardo on 11/09/2019.
//

#ifndef ALARMOBSERVER_ALARMCONTROLCENTER_H
#define ALARMOBSERVER_ALARMCONTROLCENTER_H

#include <iostream>
#include "Camera.h"
#include "WindowSensor.h"
#include "VolumetricSensor.h"
#include "Observer.h"
#include "Subject.h"

class Subject;

class AlarmControlCenter : public Observer {
public:
    void addAlarmDevice(Alarm *device) {
        alarm = device;
    }

    void anomaly() {
        std::cout << alarm->getAlarmDescription() << std::endl;
        if (dynamic_cast<Camera *>(alarm) != NULL) {
            Camera *ptr = dynamic_cast<Camera *>(alarm);

            std::cout << ptr->getVideoStream() << std::endl;
        }
        if (dynamic_cast<VolumetricSensor *>(alarm) != NULL) {
            VolumetricSensor *ptr = dynamic_cast<VolumetricSensor *>(alarm);
            std::cout << ptr->getObjectSize()<< std::endl;
        }
    }

    virtual void update() override {
        anomaly();
    }

private:
    Alarm *alarm;
};

#endif //ALARMOBSERVER_ALARMCONTROLCENTER_H
