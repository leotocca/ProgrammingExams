//
// Created by leonardo on 11/09/2019.
//

#ifndef ALARMOBSERVER_WINDOWSENSOR_H
#define ALARMOBSERVER_WINDOWSENSOR_H


#include "Alarm.h"
#include "Observer.h"
#include <list>

class WindowSensor : public Alarm {
public:
    explicit WindowSensor (std::string position) : Alarm(position){

    }
    virtual ~WindowSensor(){}

    virtual std::string getAlarmDescription() const override{
        return (position + " window opened");
    }

};


#endif //ALARMOBSERVER_WINDOWSENSOR_H
