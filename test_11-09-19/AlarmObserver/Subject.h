//
// Created by leonardo on 11/09/2019.
//

#ifndef ALARMOBSERVER_SUBJECT_H
#define ALARMOBSERVER_SUBJECT_H


#include "Observer.h"

class Observer;

class Subject {
public:
    virtual void subscribe(Observer *o) = 0;
    virtual void unsubscribe(Observer *o) = 0;
    virtual void activate() = 0;
};


#endif //ALARMOBSERVER_SUBJECT_H
