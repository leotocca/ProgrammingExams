//
// Created by leonardo on 11/09/2019.
//

#ifndef DIARYMAP_NOTE_H
#define DIARYMAP_NOTE_H

#include "Video.h"
#include "Audio.h"
#include "Image.h"
#include <list>

class Note {
public:
    Note(std::string ti, std::string te) : title(ti), text (te){}

    void add(MultimediaDocument* m){
        attach.push_back(m);
    }

    void show() const {
        for (const auto& itr: attach)
            itr->show();
    }
private:
    std::list<MultimediaDocument*> attach;
    std::string title, text;
};


#endif //DIARYMAP_NOTE_H
