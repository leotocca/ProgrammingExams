#include <iostream>
#include "Diary.h"

int main() {
    Date d1 (11,9,19, 15,31,9);
    Date d2 (12,9,19, 07,10,1);
    Video v1 ("/user/videos", "v1", 1000, 5646);
    v1.show();
    Image i1 ("/user/images", "i1", 1200);
    i1.show();
    Audio a1 ("/user/audio", "a1", 1513);
    a1.show();
    Note n1 ("n1", "Programming");
    Note n2 ("n2", "Testing");
    n1.add(&v1);
    n1.show();
    Diary diary;
    diary.add(d1,n1);
    diary.show(d1);
    diary.remove(d1);
    diary.show(d1);
}