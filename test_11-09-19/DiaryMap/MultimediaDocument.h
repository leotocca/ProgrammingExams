//
// Created by leonardo on 11/09/2019.
//

#ifndef DIARYMAP_MULTIMEDIADOCUMENT_H
#define DIARYMAP_MULTIMEDIADOCUMENT_H

#include <string>
#include <iostream>

class MultimediaDocument {
public:
    MultimediaDocument (std::string p, std::string n) : path(p), name(n){}
    virtual ~MultimediaDocument(){}

    virtual void show() = 0;
protected:
    std::string path, name;


};


#endif //DIARYMAP_MULTIMEDIADOCUMENT_H
