//
// Created by leonardo on 11/09/2019.
//


#ifndef DIRECTORY_DATE_H
#define DIRECTORY_DATE_H

#include <iostream>

class Date{
public:
    Date() : day(0), month(0), year(0), hour(0), min(0), sec(0){}
    Date(int d, int mo, int y, int h, int mi, int s) : day(d), month(mo), year(y), hour(h), min(mi), sec(s){}

    void printDate() const{
        std::cout << day << "/"<< month << "/"<< year << " : " << hour << ":" << min << ":" << sec;
    }

    bool operator<(const Date &rhs) const {
        if (day < rhs.day)
            return true;
        if (rhs.day < day)
            return false;
        if (month < rhs.month)
            return true;
        if (rhs.month < month)
            return false;
        if (year < rhs.year)
            return true;
        if (rhs.year < year)
            return false;
        if (hour < rhs.hour)
            return true;
        if (rhs.hour < hour)
            return false;
        if (min < rhs.min)
            return true;
        if (rhs.min < min)
            return false;
        return sec < rhs.sec;
    }

    bool operator>(const Date &rhs) const {
        return rhs < *this;
    }

    bool operator<=(const Date &rhs) const {
        return !(rhs < *this);
    }

    bool operator>=(const Date &rhs) const {
        return !(*this < rhs);
    }

    int day, month, year, hour, min, sec;
};




#endif //DIRECTORY_DATE_H