//
// Created by leonardo on 11/09/2019.
//

#ifndef DIARYMAP_VIDEO_H
#define DIARYMAP_VIDEO_H


#include "MultimediaDocument.h"

class Video : public MultimediaDocument {
public:
    Video (std::string p, std::string n, int d, int l) : MultimediaDocument(p,n), dim(d), length(l){}
    virtual ~Video(){}

    virtual void show() override {
        std::cout << "Name: " << name << "; Path: " << path << "; Dimension: " << dim << "; Length:" << length << std::endl;
    }
private:
    int dim, length;
};



#endif //DIARYMAP_VIDEO_H
