//
// Created by leonardo on 11/09/2019.
//

#ifndef DIARYMAP_DIARY_H
#define DIARYMAP_DIARY_H

#include "Note.h"
#include "Date.h"
#include <map>

class Diary {
public:
    void add(Date &d, Note& n) {
        diary.insert(std::make_pair(d,n));
    }

    void remove (Date&d){
        auto itr = diary.find(d);
        if (itr != diary.end())
            diary.erase(itr);
        else
            std::cout << "Date not found!" << std::endl;
    }

    void show (Date&d){
        auto itr = diary.find(d);
        if (itr != diary.end())
            itr->second.show();
        else
            std::cout << "Date not found!" << std::endl;
    }
private:
    std::map<Date,Note> diary;
};


#endif //DIARYMAP_DIARY_H
