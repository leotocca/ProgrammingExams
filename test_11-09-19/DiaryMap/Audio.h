//
// Created by leonardo on 11/09/2019.
//

#ifndef DIARYMAP_AUDIO_H
#define DIARYMAP_AUDIO_H


#include "MultimediaDocument.h"

class Audio : public MultimediaDocument {
public:
    Audio (std::string p, std::string n, int l) : MultimediaDocument(p,n), length(l){}
    virtual ~Audio(){}

    virtual void show() override {
        std::cout << "Name: " << name << "; Path: " << path << "; Lenght: " << length << std::endl;
    }
private:
    int length;
};


#endif //DIARYMAP_AUDIO_H
