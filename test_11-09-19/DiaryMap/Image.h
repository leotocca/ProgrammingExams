//
// Created by leonardo on 11/09/2019.
//

#ifndef DIARYMAP_IMAGE_H
#define DIARYMAP_IMAGE_H


#include "MultimediaDocument.h"

class Image : public MultimediaDocument{
public:
    Image (std::string p, std::string n, int d) : MultimediaDocument(p,n), dim(d){}
    virtual ~Image(){}

    virtual void show() override {
        std::cout << "Name: " << name << "; Path: " << path << "; Dimension: " << dim << std::endl;
    }
private:
    int dim;
};


#endif //DIARYMAP_IMAGE_H
