//
// Created by leonardo on 11/09/2019.
//

#ifndef FILMLIBRARY_USER_H
#define FILMLIBRARY_USER_H

#include "Library.h"
#include "Collection.h"

class User {
public:
    explicit User (string u) : user(u){}
    void add(Collection& c){
        collections.push_back(c);
    }
private:
    string user;
    list<Collection> collections;
};


#endif //FILMLIBRARY_USER_H
