//
// Created by leonardo on 11/09/2019.
//

#ifndef FILMLIBRARY_COLLECTION_H
#define FILMLIBRARY_COLLECTION_H

#include "Film.h"

class Collection {
public:
    explicit Collection(string n) : name(n){}

    void add(Film &f){
        films.push_back(f);
    }

    void remove(string n){
            for (auto& film: films){
                if (film.getTitle() == n) {
                    films.remove(film);
                    break;
                }
            }
        }

    void areDownloaded(){
        for (auto& film: films){
            if (film.isDownloaded())
                cout <<film.getTitle() << endl;
        }
    }

    void listGenres (string g){
        for (auto& film: films){
            if (film.foundGenre(g))
                cout << film.getTitle() << endl;
        }
    }
private:
    string name;
    list<Film> films;
};


#endif //FILMLIBRARY_COLLECTION_H
