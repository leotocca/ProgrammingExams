#include "User.h"

int main() {
    Film pf("Pulp Fiction", "1994", "Tarantino");
    Film svpr("Saving Private Ryan", "1999", "Spielberg");
    pf.addGenre("American crime");
    svpr.addGenre("War");
    pf.setDownloaded(true);
    Library lib;
    lib.add(pf);
    lib.add(svpr);
    lib.print();
    lib.remove("Pulp Fiction");
    lib.print();
    Collection coll ("coll1");
    coll.add(pf);
    coll.areDownloaded();
    coll.listGenres("American crime");
    coll.remove("Pulp Fiction");
    coll.areDownloaded();
    User u ("Leo");
    u.add(coll);
}