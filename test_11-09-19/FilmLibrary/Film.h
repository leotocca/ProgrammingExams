//
// Created by leonardo on 11/09/2019.
//

#ifndef FILMLIBRARY_FILM_H
#define FILMLIBRARY_FILM_H

#include <string>
#include <iostream>
#include <list>

using namespace std;


class Film {
public:
    Film (string t, string y, string d) : title(t), year (y), director(d), downloaded(false) {}

    Film() {}

    bool operator==(const Film &rhs) const {
        return title == rhs.title &&
               year == rhs.year &&
               director == rhs.director &&
               downloaded == rhs.downloaded &&
               genres == rhs.genres;
    }

    bool operator!=(const Film &rhs) const {
        return !(rhs == *this);
    }

    const string &getTitle() const {
        return title;
    }

    void setTitle(const string &title) {
        Film::title = title;
    }

    const string &getYear() const {
        return year;
    }

    void setYear(const string &year) {
        Film::year = year;
    }

    const string &getDirector() const {
        return director;
    }

    void setDirector(const string &director) {
        Film::director = director;
    }

    bool isDownloaded() const {
        return downloaded;
    }

    void setDownloaded(bool downloaded) {
        Film::downloaded = downloaded;
    }


    void addGenre(string g){
        genres.push_back(g);
    }
    bool foundGenre(string g){
        for (const auto& genre: genres){
            if (genre == g)
                return true;
        }
        return false;
    }

protected:
    string title, year, director;
    bool downloaded;
    list<string> genres;
};


#endif //FILMLIBRARY_FILM_H
