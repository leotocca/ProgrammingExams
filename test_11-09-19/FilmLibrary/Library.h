//
// Created by leonardo on 11/09/2019.
//

#ifndef FILMLIBRARY_LIBRARY_H
#define FILMLIBRARY_LIBRARY_H

#include "Film.h"

class Library {
public:
    void add(Film &f){
        films.push_back(f);
    }

    void remove(string n){
        Film temp;
        for (auto& film: films){
            if (film.getTitle() == n)
                temp = film;
        };
        films.remove(temp);
    }

    bool isFilm(string n){
        for (auto& film: films){
            if (film.getTitle() == n)
                return true;
        }
        return false;
    }

    void print() const{
        for (auto& film: films)
            cout << film.getTitle() << std::endl;
    }
private:
    list<Film> films;
};


#endif //FILMLIBRARY_LIBRARY_H
