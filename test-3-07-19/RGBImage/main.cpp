#include <iostream>
#include "Image.h"
#include "Pixel.h"

int main() {
    Pixel pix (3,22,66);
    Image img (100,100,pix);
    Pixel pix2 (2,2,2);
    img.setPixel(50,50,pix2);
    std::cout<<img.getPixel(50,50).R<<std::endl;
}