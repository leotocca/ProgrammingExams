//
// Created by leonardo on 11/08/2019.
//

#ifndef RGBIMAGE_IMAGE_H
#define RGBIMAGE_IMAGE_H

#include <string>
#include <exception>
#include <stdexcept>
#include <iostream>
#include <vector>
#include <typeinfo>
#include "zlib.h"
#include "Pixel.h"

using namespace std;

class Image{
public:
    Image () : width(0), height(0), saved(false), hash(""){
        for (unsigned i = 0; i<width*height; i++){
            buffer[i].R = 0;
            buffer[i].G = 0;
            buffer[i].B = 0;
        }
    }
    Image (unsigned x, unsigned y, Pixel& pix) : width(x), height(y), saved(false), hash(""){
        buffer = new Pixel [x*y];
        for (unsigned i = 0; i<width*height; i++){
            buffer[i] = pix;
        }
    }
    ~Image(){delete[] buffer;};
    
    Image (Image& rhs){ //copy constructor
        width = rhs.width;
        height = rhs.height;
        hash = rhs.hash;
        saved = false;
        for(int i=0; i<width*height; i++){
            buffer[i]=rhs.buffer[i];
        }
    }

    Image& operator=(const Image& rhs){
        if(this==&rhs)
            return(*this);
        width=rhs.width;
        height=rhs.height;
        delete [] buffer;
        buffer= new Pixel[width*height];
        for(int i=0; i<width*height; i++){
            buffer[i]=rhs.buffer[i];
        }
        saved=false;
        hash=rhs.hash;
        return (*this);
    }

    void save(){
        if (!saved){
            saved = true;
        }
    }
    
    void setPixel (unsigned x, unsigned y, const Pixel& pix){
        if(x<0 || y<0 || x>=width || y>=height)
            throw out_of_range("Coordinate non valide");
        buffer[x+y*width] = pix; // perché nel costruttore di copia di pixel non posso metterci il const?
        saved=false;
        hash="";
    }
    Pixel getPixel (unsigned x, unsigned y){
        if(x<0 || y<0 || x>=width || y>=height)
            throw out_of_range("Coordinate non valide");
        return buffer[x+y*width];
    }

protected:
    string computeHash(){
        if (hash != "")
            return hash;
        hash = "IM";
        string r;
        string g;
        string b;
        for(int i=0; i<width*height; i++){
            r="r"+to_string(buffer[i].R);
            g="g"+to_string(buffer[i].G);
            b="b"+to_string(buffer[i].B);
        }
        hash+=r+g+b;
        return hash;
    }

    bool operator != (Image& rhs){
        return !(hash == rhs.hash);
    }
    
    bool operator == (Image& rhs){
        return (hash == rhs.hash);
    }


private:
    unsigned width, height;
    Pixel* buffer;
    bool saved;
    string hash;
};

#endif //RGBIMAGE_IMAGE_H
