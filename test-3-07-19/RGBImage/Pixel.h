//
// Created by leonardo on 11/08/2019.
//

#ifndef RGBIMAGE_PIXEL_H
#define RGBIMAGE_PIXEL_H

struct Pixel{
    Pixel() : R(0), G(0), B(0){}
    Pixel (int r, int g, int b) : R(r), G(g), B(b){}
    Pixel (Pixel& p){
        R = p.R;
        G = p.G;
        B = p.B;
    }

    bool operator==(const Pixel &rhs) const {
        return R == rhs.R &&
               G == rhs.G &&
               B == rhs.B;
    }

    bool operator!=(const Pixel &rhs) const {
        return !(rhs == *this);
    }

    int R, G , B;
};
#endif //RGBIMAGE_PIXEL_H
