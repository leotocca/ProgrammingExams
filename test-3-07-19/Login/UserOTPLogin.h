//
// Created by leonardo on 14/08/2019.
//

#ifndef LOGIN_USEROTPLOGIN_H
#define LOGIN_USEROTPLOGIN_H

#include <iostream>
#include <memory>
#include <string>
#include <chrono>
#include "UserLogin.h"

using namespace std;

class UserOTPLogin : public UserLogin {
public:
    explicit UserOTPLogin(string user) : UserLogin(user) {}

    string sendOtp(string tNumber) {
        start = chrono::system_clock::now();
        string pass = generateRandPass();
        cout << "Sending password: '" << pass << "' to telephone number: " << tNumber << endl;
        return password;
    };

    bool checkCredentials(string user, string pass) override {
        end = chrono::system_clock::now();
        chrono::duration<double> diff = end - start;
        if (diff.count() > maxSeconds) {
            cerr << "OTP token expired. Please retry" << endl;
            return false;
        }
        if (this->username == user && this->password == pass)
            return true;
        else {
            cerr << "Wrong Username or Password. Please retry" << endl;
            return false;
        }

    }

private:
    const int maxSeconds = 60;
    chrono::time_point<chrono::system_clock> start;
    chrono::time_point<chrono::system_clock> end;
    string otpPassword;

    string generateRandPass() {
        UserLogin::password = strGen(10);
        return password;
    }

    //aggiunto
    string strGen(int n) {
        char c[n];
        static const char alphan[] =
                "0123456789"
                "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                "abcdefghijklmnopqrstvwxyz";
        for (int i = 0; i < n; i++) {
            c[i] = alphan[rand() % (sizeof(alphan))];
        }
        c[n] = 0;
        rand();
        return c;
    }
};


#endif //LOGIN_USEROTPLOGIN_H
