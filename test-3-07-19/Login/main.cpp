#include <iostream>
#include <memory>
#include "LoginAdapter.h"
#include "SmartCardLogin.h"
#include "UserOTPLogin.h"

int main() {
    
    UserLogin* userLogin (new UserOTPLogin ("Simona"));
    std::string userPass;
    std::string uTel = "3334372020";
    auto* otpLogin = dynamic_cast<UserOTPLogin*> (userLogin);
    if (otpLogin)
        userPass = otpLogin->sendOtp(uTel);
    if (userLogin->checkCredentials("Simona",userPass))
        std::cout << "logging in"<< std::endl<<std::endl<<std::endl;
    delete  userLogin;

    SmartCardLogin scrUser1 ("0xFHKODOFR");
    scrUser1.setUsername("Riccardo");
    scrUser1.setPassword("KAISER");
    SmartCardLogin* scr = &scrUser1;
    UserLogin* scrLogin = new LoginAdapter (scr,"Riccardo","KAISER");
    //std::cout << &scrUser1<< "   "<<&scrLogin<<"   "<< scrUser1.getUsername() <<std::endl<<std::endl;
    //std::cout <<scrLogin->password;
    if (scrLogin->checkCredentials("Riccardo", "KAISER")){
        std::cout << "logging in"<< std::endl<<std::endl<<std::endl;

    }


}

