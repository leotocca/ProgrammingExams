//
// Created by leonardo on 14/08/2019.
//

#ifndef LOGIN_LOGINADAPTER_H
#define LOGIN_LOGINADAPTER_H

#include "UserLogin.h"
#include "SmartCardLogin.h"

class LoginAdapter : public UserLogin {
public:
    explicit LoginAdapter(SmartCardLogin *ad, std::string us, std::string passw) : UserLogin(us) {
        adaptee = ad;
        adaptee->setUsername(us);
        adaptee->setPassword(passw);
    };

    //explicit LoginAdapter (string k) : SmartCardLogin(k){}
    virtual ~LoginAdapter() {}

    bool checkCredentials(string user, string pass) override {
        if (adaptee->isLogInOK(user, pass))
            return true;
        else {
            cout << "Username e password non corrette" << endl;
            return false;
        }
    }

private:
    SmartCardLogin *adaptee;
};


#endif //LOGIN_LOGINADAPTER_H
