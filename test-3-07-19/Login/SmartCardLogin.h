//
// Created by leonardo on 14/08/2019.
//

#ifndef LOGIN_SMARTCARDLOGIN_H
#define LOGIN_SMARTCARDLOGIN_H

#include <iostream>
#include <memory>
#include <string>
#include "UserLogin.h"

using namespace std;

class SmartCardLogin {
public:
    explicit SmartCardLogin(string key) : secretKey(key), username(""), password("") {}

    bool isLogInOK(string encryptedUser, string encryptedPassword) const {
        if (!username.empty() && !password.empty()) {
            return username == encryptedUser && password == encryptedPassword;
        } else
            return false;
    }

    void setUsername(const string &user) {
        //this->username = encryptString(user);
        this->username = user;
    }

    void setPassword(const string &pass) {
        //this->password = encryptString(pass);
        this->password = pass;
    }

    string encryptString(string str) {
        for (int i = 0; (i < 100 && str[i] != '\0'); i++)
            str[i] = str[i] + 2; //the key for encryption is 3 that is added to ASCII value

        cout << "\nEncrypted string: " << str << endl;

        return "encrypted_" + secretKey + "_" + str;
    }

    const string &getSecretKey() const {
        return secretKey;
    }

    const string &getUsername() const {
        return username;
    }

    const string &getPassword() const {
        return password;
    }

private:
    string secretKey;
    string username;
    string password;

};


#endif //LOGIN_SMARTCARDLOGIN_H
