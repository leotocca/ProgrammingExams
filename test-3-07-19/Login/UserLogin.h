//
// Created by leonardo on 14/08/2019.
//

#ifndef LOGIN_USERLOGIN_H
#define LOGIN_USERLOGIN_H

#include <iostream>
#include <memory>
#include <string>

using namespace std;

class UserLogin { //classe base astratta
public:
    explicit UserLogin (string user = "") : username (user){}
    virtual ~UserLogin (){}



    virtual bool checkCredentials(string user, string pass) = 0;

protected:
    string username;
    string password;
};


#endif //LOGIN_USERLOGIN_H
