//
// Created by leonardo on 12/08/2019.
//

#ifndef AUDIOTRACKS_AUDIOTRACK_H
#define AUDIOTRACKS_AUDIOTRACK_H

#include <string>
#include <iostream>
#include <random>

using namespace std;
class AudioTrack{
public:
    AudioTrack (string t, int d, string c) : title(t), duration(d), cover (c){}


    bool operator == (AudioTrack& rhs){
        return !(title != rhs.title || duration != rhs.duration || cover != rhs.cover);
    }

    void play(){
        cout  << title << " is playing" << endl;
    }

    const string &getTitle() const {
        return title;
    }

private:
    string title;
    int duration;
    string cover;
};
#endif //AUDIOTRACKS_AUDIOTRACK_H
