//
// Created by leonardo on 12/08/2019.
//

#ifndef AUDIOTRACKS_PLAYLIST_H
#define AUDIOTRACKS_PLAYLIST_H

#include <string>
#include <list>
#include <map>
#include "AudioTrack.h"

class Playlist{
public:
    //Playlist(){}
    explicit Playlist (string n, bool i = false): name(n), loop(i){}
    ~Playlist(){ playlist.clear();}


    void addTrack (AudioTrack& tr){
        playlist.insert(std::make_pair(tr.getTitle(),tr));
    }
    void removeTrack (string& rem){
        std::map<string, AudioTrack>::iterator itr;
        itr = playlist.find(rem);
        if (itr != playlist.end())
            playlist.erase(itr);
        else
            std::cout << rem << " This song does not exist, try again" << endl;
    }

    void playTrack (string& play){
        std::map<string, AudioTrack>::iterator itr;
        itr = playlist.find(play);
        if (itr != playlist.end())
            itr->second.play();
        else
            std::cout << play << " This song does not exist, try again" << endl;
    }

    void play(){
        for (auto itr : playlist)
            itr.second.play();
    }

    const string &getName() const {
        return name;
    }

private:
    string name;
    bool loop;
    map<string,AudioTrack> playlist;
};



#endif //AUDIOTRACKS_PLAYLIST_H