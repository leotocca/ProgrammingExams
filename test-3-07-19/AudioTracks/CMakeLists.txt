cmake_minimum_required(VERSION 3.14)
project(AudioTracks)

set(CMAKE_CXX_STANDARD 14)

add_executable(AudioTracks main.cpp AudioTrack.h Playlist.h MusicLibrary.h)