//
// Created by leonardo on 13/08/2019.
//

#ifndef AUDIOTRACKS_MUSICLIBRARY_H
#define AUDIOTRACKS_MUSICLIBRARY_H


#include <string>
#include <map>
#include <iostream>
#include "AudioTrack.h"
#include "Playlist.h"

using namespace std;

class MusicLibrary {
public:
    explicit MusicLibrary (string n) : name (n){}
    ~MusicLibrary(){playlists.clear();}

    void addPlaylist (Playlist& pl){
        playlists.insert(make_pair(pl.getName(),pl));
    }
    
    void removePlaylist (string rem){
        std::map<string, Playlist>::iterator itr;
        itr = playlists.find(rem);
        if (itr != playlists.end())
            playlists.erase(itr);
        else
            std::cout << rem << " This playlist does not exist, try again" << endl;
    }
    
    void playPlaylist (string p){
        std::map<string, Playlist>::iterator itr;
        itr = playlists.find(p);
        if (itr != playlists.end())
            itr->second.play();
        else
            std::cout << p  << " This playlist does not exist, try again" << endl;
    }

private:
    string name;
    map<string,Playlist> playlists;
};


#endif //AUDIOTRACKS_MUSICLIBRARY_H
