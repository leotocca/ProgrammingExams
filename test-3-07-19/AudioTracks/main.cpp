#include <iostream>
#include <string>
#include <map>
#include "AudioTrack.h"
#include "Playlist.h"
#include "MusicLibrary.h"

int main() {
    AudioTrack Fade_To_Black ("Fade to Black",456,"cover1");
    AudioTrack Seek_And_Destroy ("Seek and Destroy",532,"cover2");
    AudioTrack Shortest_Straw ("Shortest Straw",622,"cover3");
    AudioTrack The_Unforgiven ("The Unforgiven", 712, "cover4");
    Playlist Cliff ("Cliff", true);
    Playlist Jason ("Jason");
    Cliff.addTrack(Fade_To_Black);
    Cliff.addTrack(Seek_And_Destroy);
    Jason.addTrack(Shortest_Straw);
    Jason.addTrack(The_Unforgiven);
    MusicLibrary metallica ("Metallica");
    metallica.addPlaylist(Cliff);
    metallica.playPlaylist("Cliff");
    metallica.removePlaylist("Cliff");
    metallica.playPlaylist("Cliff");
    metallica.playPlaylist("Jason");
    metallica.addPlaylist(Jason);
    metallica.playPlaylist("Jason");

}