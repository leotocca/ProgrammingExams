//
// Created by Impresa Kapaj on 2019-09-11.
//

#ifndef ES5OBSERVER_ALARM_H
#define ES5OBSERVER_ALARM_H
using namespace std;
#include "Subject.h"
#include <string>
#include <list>
#include <iostream>

class Alarm : public Subject {
public:
    Alarm(string p): position(p){}

    virtual string getAlarmDescription() const = 0;

    virtual void activate() override {
    for(auto itr = obs.begin(); itr!= obs.end(); itr++){
            (*itr)->update(this);
    }

    }
    virtual ~Alarm(){}


    virtual void addObserver(Observer *o) override {
        obs.push_back(o);
    }
    virtual void removeObserver(Observer *o) override {
        obs.remove(o);
    }

    virtual void print() override {
        cout << "\nAlarm activated \a" << endl;
    }

protected:
    string position;
    list<Observer*> obs;

};


#endif //ES5OBSERVER_ALARM_H
