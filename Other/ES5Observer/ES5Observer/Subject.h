//
// Created by Impresa Kapaj on 2019-09-11.
//

#ifndef ES5OBSERVER_SUBJECT_H
#define ES5OBSERVER_SUBJECT_H

#include "Observer.h"

class Observer;

class Subject {
public:

    virtual void addObserver(Observer *o) = 0;
    virtual void removeObserver(Observer *o) = 0;

    virtual void activate() = 0;

    virtual void print() = 0;


};


#endif //ES5OBSERVER_SUBJECT_H
