//
// Created by Impresa Kapaj on 2019-09-11.
//

#ifndef ES5OBSERVER_OBSERVER_H
#define ES5OBSERVER_OBSERVER_H

#include "Subject.h"

class Subject;

class Observer {
public:
    virtual ~Observer(){}

    virtual void update(Subject *s) = 0;

};


#endif //ES5OBSERVER_OBSERVER_H
