//
// Created by Impresa Kapaj on 2019-09-11.
//

#ifndef ES5OBSERVER_VOLUMETRICSENSOR_H
#define ES5OBSERVER_VOLUMETRICSENSOR_H

#include <string>
#include "Alarm.h"
using namespace std;
#include <random>
#include <iostream>

class VolumetricSensor: public Alarm {
public:
    VolumetricSensor(string position) : Alarm(position) {}
    virtual ~VolumetricSensor(){}

    string getAlarmDescription() const {
        return ( position + " volumetric sensor detected a presence.");
    }

    int getObjectSize() const{
        std::random_device rd;

        std::mt19937 gen(rd());

        std::uniform_int_distribution<> dis(1,3);

        return dis(gen);
        cout << endl;
    }

};


#endif //ES5OBSERVER_VOLUMETRICSENSOR_H
