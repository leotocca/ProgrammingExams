//
// Created by Impresa Kapaj on 2019-09-11.
//

#ifndef ES5OBSERVER_WINDOWSENSOR_H
#define ES5OBSERVER_WINDOWSENSOR_H

#include <string>
#include "Alarm.h"
using namespace std;

class WindowSensor: public Alarm {
public:
    WindowSensor(string position) : Alarm(position){}
    virtual ~WindowSensor(){}

    virtual string getAlarmDescription() const{
        return( position + " window opened.");
    }

};


#endif //ES5OBSERVER_WINDOWSENSOR_H
