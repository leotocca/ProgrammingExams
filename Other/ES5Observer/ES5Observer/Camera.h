//
// Created by Impresa Kapaj on 2019-09-11.
//

#ifndef ES5OBSERVER_CAMERA_H
#define ES5OBSERVER_CAMERA_H

#include <string>
#include "Alarm.h"
using namespace std;



class Camera: public Alarm {
public:
    Camera(string position) : Alarm(position) {}
    virtual ~Camera(){}

    virtual string getAlarmDescription() const {
        return ( position + " Camera detected an anomaly.");
    }

    string getVideoStream() const {
        return ( "Let's pretend this is a video stream from " + position + " camera.");
    }

};


#endif //ES5OBSERVER_CAMERA_H
