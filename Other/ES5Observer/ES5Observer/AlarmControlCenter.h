//
// Created by Impresa Kapaj on 2019-09-11.
//

#ifndef ES5OBSERVER_ALARMCONTROLCENTER_H
#define ES5OBSERVER_ALARMCONTROLCENTER_H
#include <iostream>
#include <string>
#include "Alarm.h"
using namespace std;

#include "Camera.h"
#include "WindowSensor.h"
#include "VolumetricSensor.h"
#include "Observer.h"
#include "Subject.h"

class Subject;

class AlarmControlCenter : public Observer {
public:
    void addAlarmDevice(Alarm *device){
        alarm = device;
    }

    void deviceAnomaly(){
        cout << alarm->getAlarmDescription() << endl;
        if (dynamic_cast<Camera *>(alarm) != NULL ){
            Camera *ptr = dynamic_cast<Camera *>(alarm);

            cout << ptr->getVideoStream() << endl;
        }
        if (dynamic_cast<VolumetricSensor *>(alarm) != NULL ){
            VolumetricSensor *ptr = dynamic_cast<VolumetricSensor *>(alarm);
            cout << ptr->getObjectSize();
            }
    }


    virtual void update(Subject *s) {
        s->print();
    }

private:
    Alarm *alarm;

};


#endif //ES5OBSERVER_ALARMCONTROLCENTER_H
