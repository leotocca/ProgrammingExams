cmake_minimum_required(VERSION 3.14)
project(MediaPlayer)

set(CMAKE_CXX_STANDARD 14)

add_executable(MediaPlayer main.cpp MediaPlayer.h AudioPlayer.h AdvanceMediaPlayer.h AACPlayer.h MP3Player.h AudioAdapter.h)