//
// Created by leonardo on 30/08/2019.
//

#ifndef MEDIAPLAYER_AACPLAYER_H
#define MEDIAPLAYER_AACPLAYER_H

#include <string>
#include <iostream>
#include "AdvanceMediaPlayer.h"

class AACPlayer : public AdvanceMediaPlayer {
public:
    void play(std::string filename, bool checkDRM) override {
        std::cout << "AACPlayer is playing" << std::endl;
    }
};


#endif //MEDIAPLAYER_AACPLAYER_H
