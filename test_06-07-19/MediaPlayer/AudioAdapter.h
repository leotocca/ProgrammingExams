//
// Created by leonardo on 30/08/2019.
//

#ifndef MEDIAPLAYER_AUDIOADAPTER_H
#define MEDIAPLAYER_AUDIOADAPTER_H

#include "AdvanceMediaPlayer.h"
#include "MediaPlayer.h"
#include "AACPlayer.h"
#include "MP3Player.h"

class AudioAdapter : public MediaPlayer {
public:
    AudioAdapter(std::string str){
        if (str == "mp3")
            adaptee = new MP3Player;
        else if (str == "aac")
            adaptee = new AACPlayer;
    }
    void play (std::string audioType, std::string filename) override{
        if (audioType == "mp3")
           adaptee->play(filename,true);
        else if (audioType  == "aac")
            adaptee->play(filename,true);
        else
            std::cout <<" Unknown file format: " << audioType << std::endl;
    }
private:
    AdvanceMediaPlayer* adaptee;
};


#endif //MEDIAPLAYER_AUDIOADAPTER_H
