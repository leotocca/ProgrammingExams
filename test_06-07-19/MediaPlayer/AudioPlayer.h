//
// Created by leonardo on 30/08/2019.
//

#ifndef MEDIAPLAYER_AUDIOPLAYER_H
#define MEDIAPLAYER_AUDIOPLAYER_H

#include <string>
#include <iostream>
#include "MediaPlayer.h"
#include "AudioAdapter.h"

class AudioPlayer : public MediaPlayer {
public:

    void play (std::string audioType, std::string filename) override{
        if (audioType == "wav")
            std::cout << "AudioPlayer playing wav..." << std::endl;
        else if (audioType == "mp3" || audioType == "aac") {
            adap = new AudioAdapter(audioType);
            adap->play(audioType, filename);
        }

    }

private:
    AudioAdapter* adap;
};


#endif //MEDIAPLAYER_AUDIOPLAYER_H
