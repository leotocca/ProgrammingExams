//
// Created by leonardo on 30/08/2019.
//

#ifndef MEDIAPLAYER_MP3PLAYER_H
#define MEDIAPLAYER_MP3PLAYER_H

#include "AdvanceMediaPlayer.h"

class MP3Player : public AdvanceMediaPlayer {
    void play(std::string filename, bool checkDRM) override {
        std::cout << "MP3Player is playing" << std::endl;
    }
};

#endif //MEDIAPLAYER_MP3PLAYER_H
