//
// Created by leonardo on 30/08/2019.
//

#ifndef MEDIAPLAYER_ADVANCEMEDIAPLAYER_H
#define MEDIAPLAYER_ADVANCEMEDIAPLAYER_H

#include <string>
#include <iostream>

class AdvanceMediaPlayer {
public:
    virtual void play (std:: string filename, bool checkDRM) = 0;
};


#endif //MEDIAPLAYER_ADVANCEMEDIAPLAYER_H
