//
// Created by leonardo on 30/08/2019.
//

#ifndef MEDIAPLAYER_MEDIAPLAYER_H
#define MEDIAPLAYER_MEDIAPLAYER_H

#include <string>


class MediaPlayer {
public:
    virtual void play (std::string audioTupe, std::string filename) = 0;
protected:
    virtual ~MediaPlayer(){}
};


#endif //MEDIAPLAYER_MEDIAPLAYER_H
