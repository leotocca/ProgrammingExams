#include "DiskFile.h"
#include "DiskDirectory.h"
#include <string>
#include <iostream>

int main(){
    DiskFile file ("file.txt", 1024, true);
    DiskDirectory dir ("dir");
    dir.add(file);
    dir.print();
};