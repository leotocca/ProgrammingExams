cmake_minimum_required(VERSION 3.14)
project(DiskFile)

set(CMAKE_CXX_STANDARD 14)

add_executable(DiskFile main.cpp DiskFile.h DiskDirectory.h)