//
// Created by leonardo on 30/08/2019.
//

#ifndef DISKFILE_DISKFILE_H
#define DISKFILE_DISKFILE_H

#include <string>
#include <iostream>

class DiskFile {
public:
    DiskFile (std::string n, unsigned d, bool o) : name (n), dim (d), overwritable (o){}

    void print() const {
        std::cout << "Name: " << name << "; Dimension in bytes: " << dim << "; Is it overwritable? : " << overwritable << std::endl;
        //return "Name: " + name + "; Dimension in bytes: " + dim + "; Is it overwritable? : " + overwritable;
    }

    const std::string &getName() const {
        return name;
    }

private:
    std::string name;
    unsigned dim;
    bool overwritable;
};


#endif //DISKFILE_DISKFILE_H
