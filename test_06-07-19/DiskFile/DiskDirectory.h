//
// Created by leonardo on 30/08/2019.
//

#ifndef DISKFILE_DISKDIRECTORY_H
#define DISKFILE_DISKDIRECTORY_H

#include <string>
#include <iostream>
#include <map>
#include <list>
#include "DiskFile.h"

class DiskDirectory {
public:
    explicit DiskDirectory(std::string str) : name(str) {}

    bool operator< (const DiskDirectory& rhs){
        return this->getName() < rhs.getName();
    }

    const std::string &getName() const {
        return name;
    }

    void add (DiskFile &df) {
        fileList.push_back(df);
    }

    void add (DiskDirectory &dk){
        diskList.push_back(dk);
    }

    bool remove (std::string rem){
        bool p = false;
        for (auto file = fileList.begin(); file != fileList.end(); file++) {
            if (file->getName() == rem) {
                fileList.erase(file);
                p = true;
                break;
            }
        }
        return p;
    }

    void print() const {
        std::cout << "List of files:" << std::endl << std::endl;
        for (const auto & file : fileList){
           file.print();
           std::cout<<std::endl;
        }
        std::cout<<std::endl<<std::endl;
        std::cout << "List of directories:" << std::endl << std::endl;
        for (const auto & dir : diskList){
            dir.getName();
            std::cout<<std::endl;
        }
    }
private:
    std::string name;
    std::list <DiskFile> fileList;
    std::list <DiskDirectory> diskList;

};


#endif //DISKFILE_DISKDIRECTORY_H
