//
// Created by leonardo on 31/08/2019.
//

#ifndef PIXELIMAGE_IMAGE_H
#define PIXELIMAGE_IMAGE_H

#include <string>
#include <iostream>
#include <exception>
#include <stdexcept>
#include "RGBPixel.h"

class Image {
public:
    Image(int w, int h): width(w), height(h) {
        buffer = new RGBPixel[w*h];
        for(int i=0;i<width;i++) {
            for(int j=0; j<height; j++){
                buffer[i + j*width].R = 0;
                buffer[i + j*width].G = 0;
                buffer[i + j*width].B = 0;
            }
        }
    }
    virtual ~Image(){
        delete[] buffer;
    }

    Image& operator = (const Image &rhs){
        if (this != &rhs){
            height = rhs.height;
            width = rhs.width;
            *buffer = *rhs.buffer;
        }
        return *this;
    }

    Image (const Image &rhs){
        width = rhs.width;
        height = rhs.height;
       if (rhs.buffer != nullptr) {
           buffer = new RGBPixel(*rhs.buffer);
       }
    }
    
    RGBPixel getPixel(int w, int h){
        if (w>width || h>height || w<0 || h<0){
            throw std::out_of_range("wrong pixel coordinates");
        }
        else
            return buffer [w+h*width];
    }

    RGBPixel setPixel (int w, int h, RGBPixel pix){
        if (w>width || h>height || w<=0 || h<=0)
            throw std::out_of_range("wrong pixel coordinates");
        if (pix.R < 0 || pix.G < 0 || pix.B < 0)
            throw std::invalid_argument ( "wrong pixel values");
        buffer[w+h*width] = pix;
        std::cout << "pixel set at coordinates: " << w << "; " << h << std::endl;
    }

    RGBPixel setPixel (int w, int h, int r, int g, int b){
        if (w>width || h>height || w<=0 || h<=0)
            throw std::out_of_range("wrong pixel coordinates");
        if (r < 0 || g < 0 || b < 0)
            throw std::invalid_argument ( "wrong pixel values");
        buffer[w+h*width].R = r;
        buffer[w+h*width].G = g;
        buffer[w+h*width].B = b;
        std::cout << "pixel set at coordinates: " << w << "; " << h << std::endl;
    }


private:
    int width, height;
    RGBPixel* buffer;
};


#endif //PIXELIMAGE_IMAGE_H
