//
// Created by leonardo on 31/08/2019.
//

#ifndef PIXELIMAGE_RGBPIXEL_H
#define PIXELIMAGE_RGBPIXEL_H

#include <string>

struct RGBPixel {
    RGBPixel(): R(0), G(0), B(0) {}
    RGBPixel (int r, int g, int b) : R(r), G(g), B(b) {}
    RGBPixel (const RGBPixel& pix) : R(pix.R), G(pix.G), B(pix.B){}
    int R;
    int G;
    int B;
};


#endif //PIXELIMAGE_RGBPIXEL_H
