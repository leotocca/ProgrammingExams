#include <iostream>
#include "RGBPixel.h"
#include "Image.h"

int main() {
    RGBPixel pix1(1,2,3);
    RGBPixel pix2 (4,5,6);
    RGBPixel pix3 (pix2);
    Image ima (10,10);
    ima.setPixel(1,1,pix1);
    ima.setPixel(1,2,pix2);
    ima.setPixel(1,3,pix3);
    //std::cout<<ima.getPixel(11,20).R<<std::endl;
    ima.getPixel(1,1);
    Image im2 (ima);
    std::cout<<im2.getPixel(1,1).R;
}